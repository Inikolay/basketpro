import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;

public class QC_1878 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private String mail = "paferat527@tohup.com";
    private String pass = "12345678";

    @DataProvider(name = "shops")
    Object[][] shops() {
        return new Object[][]{
                {"de"}, {"ee"}, {"be"}, {"fi"}
        };
    }


    @Test(dataProvider = "shops")
    public void amountOfDaysInSoDescription(String shops) {
        open("https://www.autodoc." + shops + "/neolux/11767542");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .checkNumberOfDaysInSO_BlockForDeAndFrShops(shops, "200")
                .clickToNextStep()
                .login(mail, pass)
                .fillAllFieldsForShipping("autotest", "autotest", "autotest",
                        "autotest", "11111", "autotest",
                        shops, "200+002")
                .clickNextStep()
                .clickBtnBank()
                .clickBtnNextPageAllData()
                .checkNumberOfDaysInSOBlockForDeAndFrShopsAllData(shops, "200");
    }
}
