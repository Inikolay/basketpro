import aws_page.AwsLogInPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static common.Utils.PASSWORD;
import static common.Utils.eliminatorMailRandom;

public class QC_1637 extends BaseTest {


    private ProductPage productPage = new ProductPage();
    private AwsLogInPage logInPage = new AwsLogInPage();
    private String randomEmail = eliminatorMailRandom();


    @Test()
    public void amountOfDaysInSoDescription() {
        open("https://www.autodoc.de/neolux/11767542");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .registration(randomEmail, PASSWORD)
                .checkingShippingForm()
                .checkUrlPage()
                .clickLogoAutoDoc()
                .clickBtnLogOut()
                .clickLogInUserMainPage()
                .logInUserInUserProfilePage(randomEmail, PASSWORD)
                .checkIdUser();
        Assert.assertTrue(url().contains("profile/services/plus-service"));
    }

}
