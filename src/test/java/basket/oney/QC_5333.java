package basket.oney;

import org.testng.annotations.Test;
import page.BasketPage;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;


public class QC_5333 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private final String EMAIL = "kifibok769@unicsite.com";
    private final String PASSWORD = "12345678";

    @Test
    public void checkingWithDifferentTypesGoods(){
        open("https://www.auto-doc.fr/hella/930756");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn();
        productPage.closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .login(EMAIL, PASSWORD)
                .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                        "55", "10365", "Berlin",
                        "FR", "200+002")
                .clickOnCartStepLink(1, "basket");
        new BasketPage().checkPresenceOneyTooltip()
                .checkFearstAndSeacondPrice();
    }
}
