package basket;

import main_page.MainPage;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.*;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;


public class QC_5128 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private SuccessPage successPage = new SuccessPage();
    BasketPage basketPage = new BasketPage();

    private MainPage mainPage = new MainPage();
    private String mail = "nenejoh601@minterp.com";
    private String pass = "12345678";


    @Test
    public void checkingRedirectAfterLoginOutTheUser(){
        open("https://www.autodoc.de/neolux/11767542");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart();
        basketPage.clickToNextStep()
                .login(mail, pass)
                .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                        "55", "10365", "Berlin",
                        "DE", "200+002")
                .clickNextStep()
                .clickBtnBankDE()
                .clickBtnNextPageAllData()
                .clickBtnBuyOnAllData()
                .closePopupBtnInSuccessPage();
        mainPage.clickBtnLogOut();
        open("https://www.autodoc.de/elring/203328");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .checkSecurityText("SICHERE VERBINDUNG");
        Assert.assertTrue(url().contains("/basket/account"));

    }

}
