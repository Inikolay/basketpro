package utils;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;


import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.clearBrowserLocalStorage;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static utils.SetUp.setUp;

public class BaseTest {
    @BeforeClass
    public void setUpBrowser() {
        setUp();
    }

    @AfterMethod
    public void closeBrowser() {
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWebDriver();
    }
}
