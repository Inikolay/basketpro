package QC_1388_firms_ATD;

import aws_page.AwsLogInPage;
import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;

public class QC_1389 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private AwsLogInPage logInPage = new AwsLogInPage();
    private String mail = "dicid43057@tohup.com";
    private String pass = "12345678";
    private String awsEmail = "autodoc-13376";
    private String awsPassword = "u1x/0Y/G";
    private String orderID;


    @Test
    public void testFirmaPoland(){
        open("https://www.autodoc.pl/elring/203328");
        productPage.closeCookiesPopupBtn();
        orderID = productPage.clickOnBuyBtn()
            .closeRelatedPopupBtn()
            .clickOnHeaderCart()
            .clickToNextStep()
            .login(mail, pass)
            .fillAllFieldsForShippingFirma("PL", "lastName" , "test", "10-365",
                    "Kalisz", "FB-MONT A. Fułek Spółka Komandytowa",
                    "6180029941", "street", "22", "200+002")
            .clickNextStep()
            .clickBtnBank()
            .clickBtnNextPageAllData()
            .checkTextVat("23") //проверка VAT для PL
            .disableCheckboxSo()
            .clickBtnBuyOnAllData()
            .getIdOrderUser();
        open("https://aws.autodoc.de/search-orders");
        logInPage.logInAwsSearchOrders(awsEmail, awsPassword)
            .searchAndCheckOrder(orderID)
            .clickOnOrder()
            .checkVatOrder("23")
            .checkFirmConfirmation("Check manually")
            .orderIsUser();
    }
}
