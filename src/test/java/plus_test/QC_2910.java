package plus_test;

import main_page.VersandPage;
import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;

public class QC_2910 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private VersandPage versandPage = new VersandPage();
    private String mail = "paferat527@tohup.com";
    private String pass = "12345678";



    //тест не закончен
    @Test()
    public void CheckingTheDiscountForShippingToTheCountry() {
        open("https://www.autodoc.de/services/versand");
        productPage.closeCookiesPopupBtn();
        versandPage.getDeliveryPrice();
        open("https://www.autodoc.de/vaico/12246640");
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .login(mail, pass)
                .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                        "55", "10365", "Berlin",
                        "DE", "200+002")
                .clickNextStep()
                .clickBtnBankDE()
                .clickBtnNextPageAllData();
    }
}
