package plus_test;

import aws_page.AwsCustomerDataPage;
import main_page.MainPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;

import static common.Subscriptions.*;
import static common.Utils.getCurrentShopFromJS;
import static common.Utils.getRandomValue;


public class QC_3072 extends BaseTest {

    private MainPage mainPage = new MainPage();
    private AwsCustomerDataPage awsCustomerDataPage = new AwsCustomerDataPage();
    private ProductPage productPage = new ProductPage();

    private String mail = "paferat527@tohup.com";
    private String password = "12345678";
    private String userID = "99077816";
    private float safeOrderOnDeliveryPage;

    private String randomShopGrateA = getRandomValue("de", "dk", "fi", "nl");
    private String randomShopGrateB = getRandomValue("hu", "sk");
    private String randomShopGrateC = getRandomValue("bg", "cz", "gr", "pl", "ro");

    @DataProvider(name = "testSub")
    Object[] dataSubscription() {
        return new Object[][]{
                {"https://www.autodoc." + randomShopGrateA + "", gerOnlyPROSubscriptionsGreatA()},
                {"https://www.autodoc." + randomShopGrateB + "", gerOnlyPROSubscriptionsGreatB()},
                {"https://www.autodoc." + randomShopGrateC + "", gerOnlyPROSubscriptionsGreatC()},
                {"https://www.autodoc." + randomShopGrateA + "", gerOnlyEXPERTSubscriptionsGreatA()},
                {"https://www.autodoc." + randomShopGrateB + "", gerOnlyEXPERTSubscriptionsGreatB()},
                {"https://www.autodoc." + randomShopGrateC + "", gerOnlyEXPERTSubscriptionsGreatC()},
        };
    }

    @Test(dataProvider = "testSub")
    public void checkDiscountCheckForSO(String shop, String subscription) {
        open(shop);
        mainPage.closeCookiesPopupBtn();
        safeOrderOnDeliveryPage = mainPage.clickOnDelivery()
                .getSafeOrderPrice();
        awsCustomerDataPage.openCustomerView(userID)
                .chooseCustomerSubscriptionType(subscription);
        open(shop + "/vaico/12246640");
        String currentShop = getCurrentShopFromJS();
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn();
        productPage.clickOnHeaderCart()
                .clickToNextStep()
                .login(mail, password)
                .chooseDeliveryCountryForShipping(currentShop)
                .fillingPostalCodeOrDefaultFieldJSForShipping("default", currentShop)
                .writePhoneNumber("200+002")
                .clickNextStep()
                .clickBtnNextPageAllData()
                .turnOnTurnOffCheckBoxSO(true)
                .checkSoPriceSubscriptionProAndExpert(safeOrderOnDeliveryPage);
        awsCustomerDataPage.openCustomerView(userID)
                .chooseCustomerSubscriptionType("Disabled");
    }
}
