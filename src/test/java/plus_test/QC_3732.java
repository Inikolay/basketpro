package plus_test;

import org.testng.annotations.Test;
import page.ProductPage;
import utils.BaseTest;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;

public class QC_3732 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private String mail = "kifibok769@unicsite.com";
    private String pass = "12345678";
    private String prodId1, prodId2, prodId3;



    @Test
    public void CheckingTheSelectorUserWithExpertPackage(){
        open("https://www.autodoc.de/neolux/11767542");
        productPage.closeCookiesPopupBtn();
        prodId1 = productPage.clickOnBuyBtn()
                    .getProductId();
        open("https://www.autodoc.de/neolux/11762800");
        prodId2 = productPage.clickOnBuyBtn()
                .getProductId();
        open("https://www.autodoc.de/neolux/11761583");
         prodId3 = productPage.clickOnBuyBtn()
                .getProductId();
        productPage.closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .login(mail, pass)
                .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                "55", "10365", "Berlin",
                "DE", "200+002")
                .clickNextStep()
                .clickBtnBankDE()
                .clickBtnNextPageAllData()
                .clickBtnBuyOnAllData()
                .checkPopUp()
                .enterVinNumber(prodId1,"11111111111111111")
                .enterVinNumber(prodId2,"12222222222222222")
                .enterVinNumber(prodId3,"1")
                .checkTipsForVin(3,"11111111111111111", "12222222222222222")
                .enterVinNumber(prodId3, "12")
                .checkTipsForVin(3,"12222222222222222")
                .clearVinNumInput(prodId3)
                .enterVinNumber(prodId3, "3")
                .checkAbsenceTipsDropdown(3);

    }
}
