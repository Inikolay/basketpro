package plus_test;

import aws_page.AwsLogInPage;
import aws_page.ProFilerPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.BasketPage;
import page.ProductPage;
import utils.BaseTest;

import static aws_page.AwsLogInPage.EMAIL_AWS;
import static aws_page.AwsLogInPage.PASSWORD_AWS;
import static com.codeborne.selenide.Selenide.open;

public class QC_3058 extends BaseTest {


    private ProductPage productPage = new ProductPage();
    private BasketPage basketPage = new BasketPage();
    private AwsLogInPage logInPage = new AwsLogInPage();
    private ProFilerPage proFilerPage = new ProFilerPage();

    private String orderIdAWS;

    @DataProvider(name = "user_data_email_password_ID_users")
    Object[] dataEmailPasswordIDUsers() {
        return new Object[][]{
                {"kifibok769@unicsite.com", "12345678", "99077560"},
                {"jagahag552@fandua.com", "12345678", "99329498"}
        };
    }

    //дописать тест
    @Test(dataProvider ="user_data_email_password_ID_users")
    public void  checkingTheAbsenceOfFiveDiscountForUserWithATD(String email, String password, String customerId){
        open("https://www.auto-doc.fr/elring/203328?search=ELRING%20Joint%20de%20queue%20de%20soupape%20(294.110)");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart();
        basketPage.clickToNextStep()
                .login(email, password)
                .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                "55", "10365", "Berlin",
                "FR", "200+002")
                .clickNextStep()
                .clickBtnIdealCard()
                .clickBtnNextPageAllData()
                .clickBtnBuyOnAllDataGoTOCardData()
                .clickLogoAutoDoc();
        open("https://aws.autodoc.de/customer/search");
         orderIdAWS = logInPage.logInAwsForCustomerPage()
                 .writeUserId(customerId)
                .clickBtnSearch()
                .clickIdOrderCustomer()
                .clickOrderIdOnTable()
                .checkDeliveryDiscount("0")
                .getOrderIsUser();
        open("https://aws.autodoc.de/price/profiler");
         proFilerPage.writeOrderIdProFiler(orderIdAWS)
                .writeArticleIdProFiler("203328")
                .clickBtnGo()
                 .checkStandardMultiplier("1.02");
    }
}
