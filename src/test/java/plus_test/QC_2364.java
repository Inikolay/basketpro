package plus_test;

import aws_page.AwsLogInPage;
import org.testng.annotations.Test;
import page.ProductPage;
import plus_page.PlusPage;
import utils.BaseTest;

import static aws_page.AwsLogInPage.EMAIL_AWS;
import static aws_page.AwsLogInPage.PASSWORD_AWS;
import static com.codeborne.selenide.Selenide.open;

public class QC_2364 extends BaseTest {

    private PlusPage plusPage = new PlusPage();
    private ProductPage productPage = new ProductPage();
    private AwsLogInPage logInPage = new AwsLogInPage();

    private String mail = "paferat527@tohup.com";
    private String password = "12345678";

    @Test
    public void checkVatPlus(){
        open("https://www.autodoc.de/services/plus-service");
       productPage.closeCookiesPopupBtn();
       plusPage.clickBtnBuyBasisPlus()
               .logInUserlogInUserPlusAddressPage(mail, password)
               .getTextSubscriptionVatBlock("19")
               .fillAllFieldsForShipping("AUTODOC", "AG", "Josef-Orlopp-Straße",
                       "55", "10365", "Berlin", "DE", "200+002")
               .clickCheckBoxPaymentsCard()
               .clickBtnNextPageDateUserDate()
               .clickLogoAutoDoc();
        open("https://aws.autodoc.de/customer/search");
        logInPage.logInAwsForCustomerPage()
                .writeUserId("99077816")
                .clickBtnSearch()
                .clickIdOrderCustomer()
                .clickOrderIdOnTable()
                .checkCountryName("Germany")
                .checkVatOrder("19")
                .clickBtnTest()
                .clickBtnSaveChanges()
                .checkCountryName("Germany")
                .checkVatOrder("19");
    }
}
