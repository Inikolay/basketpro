package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import org.testng.Assert;


import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;


public class SuccessPage {

    private SelenideElement idOrder() {
        return $x("//div[@class='success-order__info']/b");
    }
    private SelenideElement popUp(){
        return $x("//div[@class='popup-check-vin']");
    }

    private SelenideElement inputVinNumber(String productId){
        return $x("//div[@class='vin-number']//input[@data-article-id='" + productId + "']");
    }
    public SelenideElement closePoUp(){
        return $x("//div[@class='popup-after-order']/span[@class='popup-after-order__close']");
    }
    private ElementsCollection tipsForVin(int inputIndex) {
        return $$x("(//div[@class='vin-number__dropdown'])[" + inputIndex + "]//div[@class='vin-number__dropdown-item']");
    }

    private SelenideElement tipsDropdown(int inputIndex) {
        return $x("(//div[@class='vin-number__dropdown'])[" + inputIndex + "]");
    }

    public String getIdOrderUser() {
        String getOrderId = idOrder().getText();
        return getOrderId;
    }

    public SuccessPage closePopupBtnInSuccessPage() {
        try {
            closePoUp().shouldBe(Condition.appear, ofSeconds(20));
            closePoUp().click();
            closePoUp().shouldNotBe(Condition.visible, ofSeconds(5));
            System.out.println("Cookies block closed");
        } catch (UIAssertionError e) {
            System.out.println("Cookies block doesn't appear");
        }
        return this;
    }

    public SuccessPage checkPopUp(){
        popUp().shouldBe(visible, ofSeconds(5));
        return this;
    }
    public SuccessPage enterVinNumber(String productId, String vinNum){
        inputVinNumber(productId).setValue(vinNum);
        return this;
    }

    public SuccessPage clearVinNumInput(String productID) {
        inputVinNumber(productID).scrollIntoView("{block: \"center\"}").clear();
        return this;
    }

    public SuccessPage checkTipsForVin(int inputIndex, String... expectedTipsForVin) {
        tipsForVin(inputIndex).get(0).scrollIntoView("{block: \"center\"}").shouldBe(visible);
        ArrayList<String> actualTipsForVin = new ArrayList<>();
        for (int i = 0; i < tipsForVin(inputIndex).size(); i++) {
            actualTipsForVin.add(tipsForVin(inputIndex).get(i).getText());
        }
        Assert.assertEquals(Arrays.asList(expectedTipsForVin), actualTipsForVin, "Tips for vin is not expected");
        return this;
    }

    public SuccessPage checkAbsenceTipsDropdown(int inputIndex) {
        tipsDropdown(inputIndex).shouldNotBe(visible);
        return this;
    }
}
