package page;

import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import java.time.Duration;

import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.url;

public class BasketAccountPage {
    /**
     * Локатор для поля емаил
     * */
    private SelenideElement emailInput() {
        return $x("//input[@name='Email']");
    }

    /**
     * Локатор для поля пароль
     * */
    private SelenideElement passwordInput() {
        return $x("//input[@name='Password']");
    }

    /**
     * Локатор для кнопки сабмит
     * */
    private SelenideElement submitBtn() {
        return $x("//a[@class='login']");
    }

    private SelenideElement emailRegistrationUser(){
        return $x("(//input[@id='form_Email'])[2]");
    }
    private SelenideElement passwordRegistrationUser(){
        return $x("(//input[@id='form_Password'])[2]");
    }

    private SelenideElement btnRegistrationUser(){
        return $x("(//div[@class='button-continue']/a)[2]");
    }

    private SelenideElement textSecurity(){
        return $x("//div[@class='cart-page-head__title']/span");
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Методы
     * */

    public BasketAddressPage login(String mail, String password) {
        emailInput().shouldBe(visible, Duration.ofSeconds(10)).setValue(mail);
        emailInput().shouldHave(value(mail));
        passwordInput().setValue(password);
        passwordInput().shouldHave(value(password));
        submitBtn().click();
        return page(BasketAddressPage.class);
    }

    public BasketAddressPage registration(String registrationEmail, String registrationPassword){
        emailRegistrationUser().setValue(registrationEmail);
        emailRegistrationUser().shouldHave(value(registrationEmail));
        passwordRegistrationUser().setValue(registrationPassword);
        passwordRegistrationUser().shouldHave(value(registrationPassword));
        btnRegistrationUser().shouldBe(visible).click();
        return page(BasketAddressPage.class);
    }

    public BasketAccountPage checkSecurityText(String securityText){
        String getSecurityText = textSecurity().getText();
        Assert.assertEquals(getSecurityText, securityText);
        return this;
    }
}
