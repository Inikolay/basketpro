package page;


import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class BasketPaymentsPage {


    private SelenideElement btnBank() {
        return $x("//input[@id='przelew_bankowy']");
    }

    private SelenideElement btnBankDE() {
        return $x("//input[@id='hypovereinsbank']");
    }

    private SelenideElement btnNextPageAllData() {
        return $x("//div[@class='button-continue']/a");
    }

    private SelenideElement btnIdealCard() {
        return $x("//input[@id='adyen_credit_cards']");
    }

    public BasketPaymentsPage clickBtnBank() {
        btnBank().shouldBe(visible).click();
        return this;
    }

    public BasketAllDataPage clickBtnNextPageAllData() {
        btnNextPageAllData().shouldBe(visible).click();
        return page(BasketAllDataPage.class);
    }

    public BasketPaymentsPage clickBtnBankDE() {
        btnBankDE().shouldBe(visible).click();
        return this;
    }

    public BasketPaymentsPage clickBtnIdealCard() {
        btnIdealCard().click();
        return this;
    }
}
