package page;

import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class BasketPage {

    private SelenideElement soInfoBlock() {
        return $x("//div[contains(@class,'bestelen-block__col--text')]");
    }

    /**
     * Локатор для конпки Следующий шаг
     */
    private SelenideElement nextStepBtn() {
        return $x("//a[@data-ga-action='Next_click']");
    }

    private SelenideElement tolTipOneyBtn() {
        return $x("//div[@class='oney-text oney-tooltip-btn']/p");
    }

    private SelenideElement oneyTooltipImages() {
        return $x("//div[@class='oney-tooltip__images']");
    }

    private SelenideElement priceApport() {
        return $x("(//div[@class='oney-tooltip__apport-text']/b)[1]");
    }

    private SelenideElement priceFirstMensualite() {
        return $x("(//div[@class='oney-tooltip__apport-text']/b)[2]");
    }

    private SelenideElement pricSeconsMensualite() {
        return $x("(//div[@class='oney-tooltip__apport-text']/b)[3]");
    }

    private SelenideElement priceFinansmant() {
        return $x("(//div[@class='oney-tooltip__apport-text'])[4]");
    }

    /**
     * Локаторы для блока order samari
     */

    private SelenideElement orderTotal() {
        return $x("//span[@class='order-grand-total']");
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Методы
     *
     */

    public float getTotalPrice(){
        return Float.parseFloat(orderTotal().getOwnText()
                .replaceAll("[^\\d,]", "")
                .replaceAll(",", "."));
    }
    public BasketPage checkNumberOfDaysInSO_BlockForDeAndFrShops(String shop, String numberDays) {
        if (shop.equalsIgnoreCase("DE") || shop.equalsIgnoreCase("FR")) {
            String actualNumberDays = soInfoBlock().getText().replaceAll("\\D", "");
            Assert.assertTrue(actualNumberDays.equals(numberDays), "Expected amount of days does not coincides with actual!");
        } else {
            System.out.println("Shop is not equals to DE and FR");
        }
        return this;
    }

    public BasketAccountPage clickToNextStep() {
        nextStepBtn().click();
        return page(BasketAccountPage.class);
    }

    private BasketPage clickTolTipOneyBtn() {
        tolTipOneyBtn().click();
        return this;
    }

    public BasketPage checkPresenceOneyTooltip() {
        tolTipOneyBtn().scrollIntoView("{block: \"center\"}").click();
        oneyTooltipImages().shouldBe(visible);
        return this;
    }

    public BasketPage checkFearstAndSeacondPrice() {
        String thirdOfTheTotalPrice = String.valueOf((getTotalPrice() / 3)).replaceAll("(^\\d+.\\d{2}\\d+)", "$1");

        Assert.assertEquals(Float.parseFloat(thirdOfTheTotalPrice), Float.parseFloat(priceFirstMensualite()
                .getText().replaceAll("[^\\d,]", "")
                .replaceAll(",", ".")), 0.02f);
        Assert.assertEquals(Float.parseFloat(thirdOfTheTotalPrice), Float.parseFloat(pricSeconsMensualite()
                .getText().replaceAll("[^\\d,]", "")
                .replaceAll(",", ".")),0.02f);
        return this;
    }

    public BasketPage checkComisionPriceFinancing(){
        String costOfFinancing = String.valueOf((getTotalPrice() * 1.45f / 100)).replaceAll("(^\\d+.\\d{2}\\d+)", "$1");
        return this;
    }
}
