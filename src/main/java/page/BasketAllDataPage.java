package page;


import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import plus_page.CardDatUserPage;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;


public class BasketAllDataPage {
    private SelenideElement sOElementBlock() {
        return $x("//div[contains(@class,'bestelen-block__col--text')]");
    }

    private SelenideElement btnBuyOnAllData() {
        return $x("//div[@class='order-summary__button alldata-submit']/a");
    }

    private SelenideElement checkBoxSo() {
        return $x("//div[@class='bestelen-block__col']/input[@name='security_delivery']");
    }

    private SelenideElement textVat() {
        return $x("//span[@class='order-grand-total']/i");
    }

    private SelenideElement orderGrandTotal() {
        return $x("//span[@class='order-grand-total']");
    }

    private SelenideElement priceDeliveryOnAllData() {
        return $x("//div[@class='order-summary__row']/span[@class='order-delivery'][2]");
    }

    private SelenideElement safaOrderBlock() {
        return $x("//div[@class='order-summary ']//div[4]//span[2]");
    }

    private SelenideElement SoPrice() {
        return $("//div[@class='bestelen-block__col']/label");
    }


    public BasketAllDataPage checkNumberOfDaysInSOBlockForDeAndFrShopsAllData(String shop, String numberDays) {
        if (shop.equals("DE") || shop.equals("FR")) {
            String actualNumberDays = sOElementBlock().getText().replaceAll("\\D", "");
            Assert.assertTrue(actualNumberDays.equals(numberDays), "Expected amount of days does not coincides with actual!");
        } else {
            System.out.println("Shop is not equals to DE and FR");
        }
        return this;
    }

    /**
     * Метод для проверки VAT для фирмы PL
     **/

    public BasketAllDataPage checkTextVat(String vatOrder) {
        String getTextVat = textVat().getText().replaceAll("\\D", "");
        Assert.assertTrue(getTextVat.equals(vatOrder), "Not the first VAT number");
        return this;
    }

    public BasketAllDataPage disableCheckboxSo() {
        checkBoxSo().shouldBe(visible).click();
        return this;
    }

    public String getOrderGrandTotal() {
        String getGrandTotalAllData = orderGrandTotal().getText();
        System.out.println(getGrandTotalAllData);
        return getGrandTotalAllData;
    }

    public SuccessPage clickBtnBuyOnAllData() {
        btnBuyOnAllData().shouldBe(visible).click();
        return page(SuccessPage.class);
    }

    public CardDatUserPage clickBtnBuyOnAllDataGoTOCardData() {
        btnBuyOnAllData().shouldBe(visible).click();
        return page(CardDatUserPage.class);

    }

    public BasketAllDataPage turnOnTurnOffCheckBoxSO(boolean switcher) {
        boolean status = checkBoxSo().has(attribute("checked"));
        if (switcher) {
            if (status) {
                checkBoxSo().shouldNotHave(attribute("checked"));
            } else {
                checkBoxSo().click();
                sleep(3000);
            }
            checkBoxSo().has(attribute("checked"));
            safaOrderBlock().shouldBe(visible, ofSeconds(5));
        } else {
            if (!status) {
                checkBoxSo().shouldNotHave(attribute("checked"));
            } else {
                checkBoxSo().click();
                sleep(3000);
            }
            checkBoxSo().shouldNotHave(attribute("checked"));
            safaOrderBlock().shouldNotBe(visible, ofSeconds(5));
        }
        return this;
    }

    public float getSOPriceFromExpertBlock(SelenideElement locatorForBlock) {
        locatorForBlock.shouldBe(visible);
        String soPrice = locatorForBlock.getText();
        return Float.parseFloat(soPrice.substring(0, soPrice.indexOf(" ")).replaceAll(",", "."));
    }

    public BasketAllDataPage checkSoPriceSubscriptionProAndExpert(float realSo) {
        float discountPriceSO = realSo * 0.5f;
        float soPriceInSoBlock = getSOPriceFromExpertBlock(SoPrice());
        float soPriceInTotalBlock = getSOPriceFromExpertBlock(safaOrderBlock());
        Assert.assertEquals(discountPriceSO, soPriceInSoBlock, 0.5f, "SO wiht discount is not equal price in SO block ");
        Assert.assertEquals(discountPriceSO, soPriceInTotalBlock, 0.5f, "SO wiht discount is not equal price in samary block");
        return this;
    }
}
