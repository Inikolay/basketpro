package page;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import main_page.MainPage;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static common.Utils.checkingContainsUrl;
import static java.time.Duration.*;

public class BasketAddressPage {

    private SelenideElement lastName() {
        return $x("//input[@name='lVorname']");
    }

    private SelenideElement firstName() {
        return $x("//input[@name='lName']");
    }

    private SelenideElement street() {
        return $x("//input[@name='lStrasse']");
    }

    private SelenideElement deliveryHouse() {
        return $x("//input[@name='delivery_house']");
    }

    private SelenideElement zipCode() {
        return $x("//input[@name='lPlz']");
    }

    private SelenideElement city() {
        return $x("//input[@name='lOrt']");
    }

    public SelenideElement telephoneNumber() {
        return $x("//input[@name='lTelefon']");
    }

    private SelenideElement deliveryCountry(String country) {
        return $x("//*[@name='lLand']//*[@data-code='" + country + "']");
    }

    private SelenideElement checkBoxFirma() {
        return $x("//input[@class='company-checkbox']");
    }

    private SelenideElement firmaName() {
        return $x("//input[@id='form_lFirma']");
    }

    private SelenideElement idVatFirma() {
        return $x("//input[@id='form_lUmsatzId']");
    }

    private SelenideElement checkBoxShowBilling() {
        return $x("//input[@id='showBilling']");
    }

    private SelenideElement logoAutoDoc() {
        return $x("//div[@class='cart-page-head__logo']/a");
    }

    private SelenideElement cartStepLink(int numberStep) {
        return $x("//li[@data-step='" + numberStep + "']");
    }



    /**
     * Локатор для кнопки Следующий шаг
     */
    private SelenideElement nextStepBtn() {
        return $x("//div[@class='button-continue address-continue']/a");
    }

    private SelenideElement shippingForm() {
        return $x("//div[@class='shipping-form input-form']");
    }


    //------------------------------------------------------------------------------------------------------------------

    public void checkCorrectTextAndFillInput(SelenideElement element, String correctText) {
        Configuration.fastSetValue = false;
        if (!element.getValue().equals(correctText)) {
            element.clear();
            element.setValue(correctText);
        }
    }

    public BasketAddressPage fillAllFieldsForShipping(String lastName, String firstName, String street, String deliveryHouse,
                                                      String zipCode, String city, String country, String telephoneNumber) {
        checkCorrectTextAndFillInput(lastName(), lastName);
        checkCorrectTextAndFillInput(firstName(), firstName);
        checkCorrectTextAndFillInput(street(), street);
        checkCorrectTextAndFillInput(deliveryHouse(), deliveryHouse);
        checkCorrectTextAndFillInput(zipCode(), zipCode);
        checkCorrectTextAndFillInput(city(), city);
        chooseDeliveryCountryForShipping(country);
        checkCorrectTextAndFillInput(telephoneNumber(), telephoneNumber);
        return this;
    }

    public BasketAddressPage fillingPostalCodeOrDefaultFieldJSForShipping(String postalCodeOrCodeDefault, String shop) {
        if (postalCodeOrCodeDefault.equals("default")) {
            switch (shop) {
                case "DK":
                case "SI":
                case "AT":
                case "LD":
                    postalCodeOrCodeDefault = "1234";
                    break;
                case "NL":
                    postalCodeOrCodeDefault = "1234 AA";
                    break;
                case "PT":
                    postalCodeOrCodeDefault = "1234-567";
                    break;
                case "GR":
                case "SK":
                case "CZ":
                case "SE":
                    postalCodeOrCodeDefault = "123 45";
                    break;
                default:
                    postalCodeOrCodeDefault = "12345";
                    break;
            }
        }
        zipCode().shouldBe(visible, ofSeconds(10));
        JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
        js.executeScript("arguments[0].value='" + postalCodeOrCodeDefault + "';", zipCode());
        return this;
    }

    public BasketAddressPage clickCheckBoxFirma() {
        checkBoxFirma().shouldBe(visible).click();
        return this;
    }

    /***
     * Мутод для заполнения фирмы
     * **/
    public BasketAddressPage fillAllFieldsForShippingFirma(String country, String lastName, String firstName,
                                                           String zipCode, String city, String firmaName, String idVatFirma,
                                                           String street, String deliveryHouse, String telephoneNumber) {
        chooseDeliveryCountryForShipping(country);
        checkCorrectTextAndFillInput(lastName(), lastName);
        checkCorrectTextAndFillInput(firstName(), firstName);
        checkCorrectTextAndFillInput(zipCode(), zipCode);
        checkCorrectTextAndFillInput(city(), city);
        checkCorrectTextAndFillInput(firmaName(), firmaName);
        checkCorrectTextAndFillInput(idVatFirma(), idVatFirma);
        checkCorrectTextAndFillInput(street(), street);
        checkCorrectTextAndFillInput(deliveryHouse(), deliveryHouse);
        checkCorrectTextAndFillInput(telephoneNumber(), telephoneNumber);
        return this;
    }

    public BasketAddressPage writePhoneNumber(String phoneNumber){
        telephoneNumber().setValue(phoneNumber);
        return this;
    }

    public BasketAddressPage chooseDeliveryCountryForShipping(String country) {
        deliveryCountry(country).shouldBe(visible).click();
        return this;
    }

    public BasketPaymentsPage clickNextStep() {
        nextStepBtn().click();
        return page(BasketPaymentsPage.class);
    }

    public BasketAddressPage clickCheckBoxShowBilling() {
        checkBoxShowBilling().shouldBe(visible).click();
        return this;
    }

    public BasketAddressPage checkUrlPage() {
        Assert.assertTrue(url().contains("/basket/address"));
        return this;
    }

    public MainPage clickLogoAutoDoc() {
        logoAutoDoc().shouldBe(visible, ofSeconds(5)).click();
        return page(MainPage.class);
    }

    public BasketAddressPage checkingShippingForm() {
        shippingForm().shouldBe(visible, ofSeconds(10));
        return this;
    }

    public void clickOnCartStepLink(int numberStep, String expectedUrl) {
        cartStepLink(numberStep).click();
        checkingContainsUrl(expectedUrl);
    }


}
