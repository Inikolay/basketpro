package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import common.Cookies;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class ProductPage implements Cookies {
    private SelenideElement headerCart() {
        return $x("//div[@class='header-cart__count']");
    }

    /**
     * Локатор для кнопки купить
     */
    private SelenideElement buyBtn() {
        return $x("//div[@class='product-button button ']");
    }

    /**
     * Локатор кнопки закрытия попапа альтернатив
     */
    private SelenideElement closePopupRelatedBtn() {
        return $x("//div[contains(@class,'popup-related-goods__close')]");
    }
    private SelenideElement prodID(){
        return $x("//div[@class='product-button button ']");
    }

    /**
     * Методы
     */
    public ProductPage clickOnBuyBtn() {
        buyBtn().click();
        return this;
    }

    public ProductPage closeRelatedPopupBtn() {
        try {
            closePopupRelatedBtn().shouldBe(Condition.appear, Duration.ofSeconds(20));
            closePopupRelatedBtn().click();
            closePopupRelatedBtn().shouldNotBe(Condition.visible, Duration.ofSeconds(5));
            System.out.println("Related popup closed");
        } catch (UIAssertionError e) {
            System.out.println("Related popup doesn't appear");
        }
        return this;
    }

    public BasketPage clickOnHeaderCart() {
        headerCart().scrollIntoView("{block:\"center\"}").click();
        return page(BasketPage.class);
    }

    public String getProductId(){
        return prodID().getAttribute("id");
    }
}


