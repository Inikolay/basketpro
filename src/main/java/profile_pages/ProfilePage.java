package profile_pages;


import com.codeborne.selenide.SelenideElement;



import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;

public class ProfilePage {
  private SelenideElement topTitleUser(){
      return $x("//div[@class='top_title']/span");
  }

  public ProfilePage checkIdUser(){
      topTitleUser().shouldBe(visible, ofSeconds(5));
      return this;
  }
}
