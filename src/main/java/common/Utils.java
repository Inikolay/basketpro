package common;

import com.codeborne.selenide.ex.TimeoutException;
import org.testng.Assert;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import static com.codeborne.selenide.Configuration.pageLoadTimeout;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;


public class Utils {
    public static final String PASSWORD  = "!q123456789Q";

    public static String eliminatorMailRandom() {
        Random randomGenerator = new Random();
        int random = randomGenerator.nextInt();
        return "autotest" + random + "@mailinator.com";
    }

    public static void checkingContainsUrl(String expectedContainsUrl) {
        pageLoadTimeout = 200000;
        try {
            Wait().until(webDriver -> url().contains(expectedContainsUrl));
        } catch (TimeoutException e) {
            System.out.println(url());
            Assert.fail("Url doesn't contains: " + expectedContainsUrl);
        }
    }

    public static String getRandomValue(String...values){
        List<String> listOfValues = Arrays.asList(values);
        Collections.shuffle(listOfValues);
        return listOfValues.get(0);
    }

    public static String getCurrentShopFromJS(){
        String currentShop = executeJavaScript("return $siteSettings.lang");
        return currentShop.toUpperCase();
    }
}
