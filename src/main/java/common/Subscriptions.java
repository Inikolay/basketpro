package common;

import static common.Utils.getRandomValue;

public class Subscriptions {
    public static String gerOnlyPROSubscriptionsGreatA() {
        return getRandomValue(
                "(Grade A) T+ (PROFESSIONAL TRIAL plus) 30 DAY", "(Grade A) P+ (PROFESSIONAL plus) MONTH",
                "(Grade A) P+ (PROFESSIONAL plus) YEAR", "(Grade A) P+ (PROFESSIONAL plus) HALF YEAR", "(Grade A) P+ (PROFESSIONAL plus) TWO YEAR",
                "(Grade A) P+ (PROFESSIONAL plus) FIVE YEAR"
        );
    }

    public static String gerOnlyPROSubscriptionsGreatB() {
        return getRandomValue(
                "(Grade B) T+ (PROFESSIONAL TRIAL plus) 30 DAY",
                "(Grade B) P+ (PROFESSIONAL plus) MONTH", "(Grade B) P+ (PROFESSIONAL plus) YEAR", "(Grade B) P+ (PROFESSIONAL plus) HALF YEAR",
                "(Grade B) P+ (PROFESSIONAL plus) TWO YEAR", "(Grade B) P+ (PROFESSIONAL plus) FIVE YEAR"
        );
    }

    public static String gerOnlyPROSubscriptionsGreatC() {
        return getRandomValue(
                "(Grade C) T+ (PROFESSIONAL TRIAL plus) 30 DAY", "(Grade C) P+ (PROFESSIONAL plus) MONTH",
                "(Grade C) P+ (PROFESSIONAL plus) YEAR", "(Grade C) P+ (PROFESSIONAL plus) HALF YEAR", "(Grade C) P+ (PROFESSIONAL plus) TWO YEAR",
                "(Grade C) P+ (PROFESSIONAL plus) FIVE YEAR"
        );
    }

    public static String gerOnlyEXPERTSubscriptionsGreatA() {
        return getRandomValue(
                "(Grade A) T+ (EXPERT TRIAL plus) 30 DAY", "(Grade A) E+ (EXPERT plus) MONTH",
                "(Grade A) E+ (EXPERT plus) YEAR", "(Grade A) E+ (EXPERT plus) HALF YEAR",
                "(Grade A) E+ (EXPERT plus) TWO YEAR", "(Grade A) E+ (EXPERT plus) FIVE YEAR"
        );
    }

    public static String gerOnlyEXPERTSubscriptionsGreatB() {
        return getRandomValue(
                "(Grade B) T+ (EXPERT TRIAL plus) 30 DAY",
                "(Grade B) E+ (EXPERT plus) MONTH", "(Grade B) E+ (EXPERT plus) YEAR", "(Grade B) E+ (EXPERT plus) HALF YEAR",
                "(Grade B) E+ (EXPERT plus) TWO YEAR", "(Grade B) E+ (EXPERT plus) FIVE YEAR"
        );
    }

    public static String gerOnlyEXPERTSubscriptionsGreatC() {
        return getRandomValue(
                "(Grade C) T+ (EXPERT TRIAL plus) 30 DAY", "(Grade C) E+ (EXPERT plus) MONTH",
                "(Grade C) E+ (EXPERT plus) YEAR", "(Grade C) E+ (EXPERT plus) HALF YEAR", "(Grade C) E+ (EXPERT plus) TWO YEAR",
                "(Grade C) E+ (EXPERT plus) FIVE YEAR"
        );
    }
}
