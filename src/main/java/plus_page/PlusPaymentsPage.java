package plus_page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class PlusPaymentsPage {
    private SelenideElement checkBoxPaymentsCard(){
        return $x("//input[@id='be2bill_visa_mc']");
    }
    private SelenideElement btnNextPageDateUserDate(){
        return $x("//div[@id='payment-block']/a[@id='move-to-process']");
    }

    public PlusPaymentsPage clickCheckBoxPaymentsCard(){
        checkBoxPaymentsCard().shouldBe(visible).click();
        return this;
    }

    public CardDatUserPage clickBtnNextPageDateUserDate(){
        btnNextPageDateUserDate().shouldBe(visible).click();
        return page(CardDatUserPage.class);
    }
}
