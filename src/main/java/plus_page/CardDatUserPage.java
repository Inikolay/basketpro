package plus_page;

import aws_page.AwsLogInPage;
import com.codeborne.selenide.SelenideElement;

import java.io.IOException;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;


public class CardDatUserPage {

    private final String CUSTOMER_VIEW_URL = "https://aws.autodoc.de/customer/view/";

    private SelenideElement logoAutoDoc() {
        return $x("//div[@class='cart-page-head__logo']/a");
    }

    public AwsLogInPage clickLogoAutoDoc() {
        logoAutoDoc().shouldBe(visible).click();
        return page(AwsLogInPage.class);
    }

}
