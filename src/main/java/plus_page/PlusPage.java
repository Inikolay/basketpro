package plus_page;

import com.codeborne.selenide.SelenideElement;
import main_page.AuthorisationPopPapLogIn;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class PlusPage {
    private SelenideElement btnBuyBasisPlus(){
        return $x("//div[@class='atd-plus-services__item-start']/a[1]");
    }

    public AuthorisationPopPapLogIn clickBtnBuyBasisPlus(){
        btnBuyBasisPlus().shouldBe(visible).click();
        return page(AuthorisationPopPapLogIn.class);
    }
}
