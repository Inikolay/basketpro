package plus_page;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class PlusAddressPage {

    private SelenideElement subscriptionVatBlock(){
        return $x("//div[@id='subscription_vat_block']");
    }

    private SelenideElement lastName(){
        return $x("//input[@id='form_rVorname']");
    }

    private SelenideElement name(){
        return $x("//input[@id='form_rName']");
    }
    private SelenideElement address(){
        return $x("//input[@id='form_rStrasse']");
    }

    private SelenideElement numberHous(){
        return $x("//input[@id='form_payment_house']");
    }
    private SelenideElement postalCode(){
        return $x("//input[@id='form_rPlz']");
    }
    private SelenideElement city(){
        return $x("//input[@id='form_rOrt']");
    }

    private SelenideElement countryDelivery(String country){
        return $x("//*[@name='rLand']//*[@data-code='" + country + "']");
    }
    private SelenideElement phone(){
        return $x("//input[@id='form_rTelefon']");
    }

    private SelenideElement nextStepPayments(){
        return $x("//div[@id='subscription_step2']/a");
    }

    public PlusAddressPage getTextSubscriptionVatBlock(String vatCountryOrder){
        String getTextSubscription = subscriptionVatBlock().getText().replaceAll("\\D", "");
        Assert.assertTrue(getTextSubscription.equals(vatCountryOrder), "Not the first VAT number");
        return this;
    }

    private void checkCorrectTextAndFillInput(SelenideElement element, String correctText) {
        Configuration.fastSetValue = false;
        if (!element.getValue().equals(correctText)) {
            element.clear();
            element.setValue(correctText);
        }
    }

    public PlusPaymentsPage fillAllFieldsForShipping(String lastName, String name, String address, String numberHous,
                                                      String postalCode, String city, String country, String phone) {
        checkCorrectTextAndFillInput(lastName(), lastName);
        checkCorrectTextAndFillInput(name(), name);
        checkCorrectTextAndFillInput(address(), address);
        checkCorrectTextAndFillInput(numberHous(), numberHous);
        checkCorrectTextAndFillInput(postalCode(), postalCode);
        checkCorrectTextAndFillInput(city(), city);
        chooseDeliveryCountryForShipping(country);
        checkCorrectTextAndFillInput(phone(), phone);
        nextStepPayments().shouldBe(visible).click();
        return page(PlusPaymentsPage.class);
    }

    public PlusAddressPage chooseDeliveryCountryForShipping(String country) {
        countryDelivery(country).shouldBe(visible).click();
        return this;
    }


}
