package aws_page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class AwsSearchCustomerPage {

    private SelenideElement searchUserId(){
        return $x("//input[@id='form_Filter[id]']");
    }
    private SelenideElement searchUserEmail(){
        return $x("//input[@id='form_Filter[id]']");
    }
    private SelenideElement btnSearch(){
        return $x("//input[@id='form_search']");
    }

    private SelenideElement idOrderCustomer(){
        return $x("//td[@class='customer-id-cell']/a[2]");
    }


    public AwsSearchCustomerPage writeUserId(String userId){
        searchUserEmail().setValue(userId);
        return this;
    }

    public AwsSearchCustomerPage clickBtnSearch(){
        btnSearch().shouldBe(visible).click();
        return this;
    }

    public AwsCustomerDataPage clickIdOrderCustomer(){
        idOrderCustomer().shouldBe(visible).click();
        return page(AwsCustomerDataPage.class);
    }
}
