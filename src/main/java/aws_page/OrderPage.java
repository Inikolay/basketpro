package aws_page;

import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class OrderPage {

    private SelenideElement customerId(){
        return $x("//div[@class='data-text']/a");
    }
    private SelenideElement vatInput(){
        return $x("//li[@id='form_Order_isVatSelect__chzn_c_0']/span");
    }

    private SelenideElement firmConfirmation(){
        return $x("//select[@id='form_Order[umsatCheck]']/option[1]");
    }
    private SelenideElement btnSaveChanges(){
        return $x("//div[@class='form-actions order-panel']/button[@type='button']");
    }
    private SelenideElement countryName(){
        return $x("//select[@id='form_Order[payment_country_id]']/option[@data-code='DE']");
    }
    private SelenideElement btnTest(){
        return $x("//button[@class='btn btn-primary']");
    }
    private SelenideElement deliveryPrice(){
        return $x("//td[@class='inf_deliveryCost']/a[@class='payment-in-order']");
    }
    private SelenideElement discountText(){
        return $x("//th/b[@class='inf_discount']");
    }

    private SelenideElement articleID(){
        return $x("//td[@class='center article-id-re-order-field ']/a");
    }

    private SelenideElement orderId(){
        return $x("//td[@class='inf_orderId']");
    }

    public OrderPage checkVatOrder(String vatId){
        String getTextVat = vatInput().getText().replaceAll("\\D", "");
        Assert.assertTrue(getTextVat.equals(vatId), "Not the first VAT number");
        return this;
    }

    public OrderPage checkFirmConfirmation(String firmConfirmationText){
        String getTextFirmConfirmation = firmConfirmation().getText();
        Assert.assertEquals(getTextFirmConfirmation, firmConfirmationText);
        return this;
    }

    public OrderPage clickBtnSaveChanges(){
        btnSaveChanges().shouldBe(visible).click();
        return this;
    }

    public AwsCustomerDataPage orderIsUser(){
        orderId().shouldBe(visible).click();
        return page(AwsCustomerDataPage.class);
    }

    public String getOrderIsUser() {
        String getOrderIdAWS = orderId().getText();
        return getOrderIdAWS;
    }
    public String getArticleItems() {
        String articleIDAWS = orderId().getText();
        return articleIDAWS;
    }

    public OrderPage checkCountryName(String countryName){
        String getCountryName = countryName().getText();
        Assert.assertEquals(getCountryName, countryName);
        return this;
    }
    public OrderPage clickBtnTest(){
        btnTest().scrollIntoView(true).shouldBe(visible).click();
        return this;
    }
    public OrderPage checkDeliveryPrice(String deliveryPrice){
       String getDeliveryPrice = deliveryPrice().getText();
       Assert.assertEquals(getDeliveryPrice, deliveryPrice);
       return this;
    }
    public OrderPage checkDeliveryDiscount(String textDiscount){
        String getTextDiscount = discountText().getText();
        Assert.assertEquals(getTextDiscount, textDiscount);
        return this;
    }
}
