package aws_page;


import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import page.SuccessPage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;


public class AwsSearchOrdersPage extends SuccessPage {

    private SelenideElement inputOrderId(){
        return $x("//input[@id='form_Filter[orderId]']");
    }

    private SelenideElement btnSearchOrders(){
        return $x("//div[@class='form-group']/button[@name='search']");
    }

    private  SelenideElement numberOrderId(){
        return $x("//a[@class='order_link']");
    }


    /**
     * Проверка номера ID
     * **/
    public AwsSearchOrdersPage searchAndCheckOrder(String orderID){
        inputOrderId().setValue(orderID);
        btnSearchOrders().shouldBe(visible).click();
        String getOrderId = numberOrderId().getText();
        Assert.assertEquals(orderID, getOrderId);
        return this;
    }

    public OrderPage clickOnOrder(){
        numberOrderId().shouldBe(visible).click();
        return page(OrderPage.class);
    }

}
