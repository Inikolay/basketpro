package aws_page;


import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Billing Address
 * */

public class AwsOrderPage {
    private SelenideElement customerID(){
        return $x("//input[id='Order[CustomerID]']");

    }

    private SelenideElement selectAnOption(){
        return $x("//div[@class='form_Order_orderProjectId__chzn']");
    }

    private SelenideElement search(){
        return $x("//div[@class='chzn-search']/input[@type='text']");
    }

    private SelenideElement nameBilling(){
        return $x("//input[@id='Order[rVorname]']");
    }

    private SelenideElement lastNameBilling(){
       return  $x("//input[@id='Order[rName]']");
    }

    private SelenideElement companyBilling(){
        return $x("//input[@id='Order[Firma]']");
    }

    private SelenideElement companyIdBilling(){
        return $x("//input[@id='Order[rUmsatzId]']");
    }

    private SelenideElement streetBilling(){
        return $x("//input[@id='Order[rStrasse]']");
    }

    private SelenideElement numberHouseBilling(){
        return $x("//input[@id='Order[payment_house]']");
    }

    private SelenideElement postCodeBilling(){
        return $x("//input[@id='Order[rPlz]']");
    }

    private SelenideElement cityBilling(){
        return $x("//input[@id='Order[rOrt]']");
    }

    private SelenideElement countryBilling(String countryBilling){
        return $x("//*[@id='form_Order[payment_country_id]']//*[@data-code='" + countryBilling + "']");
    }

    private SelenideElement emailBilling(){
        return $x("//input[@id='Order[Email]']");
    }

    private SelenideElement phoneBilling(){
        return $x("//input[@id='Order[rTelefon]']");
    }

    /**
     * Delivery Address
     * */

    private SelenideElement nameDelivery (){
        return $x("//input[@id='Order[rVorname]']");
    }

    private SelenideElement lastNameDelivery (){
        return  $x("//input[@id='Order[rName]']");
    }

    private SelenideElement companyDelivery (){
        return $x("//input[@id='Order[Firma]']");
    }

    private SelenideElement companyIdDelivery (){
        return $x("//input[@id='Order[rUmsatzId]']");
    }

    private SelenideElement streetDelivery (){
        return $x("//input[@id='Order[rStrasse]']");
    }

    private SelenideElement numberHouseDelivery (){
        return $x("//input[@id='Order[payment_house]']");
    }

    private SelenideElement postCodeDelivery (){
        return $x("//input[@id='Order[rPlz]']");
    }

    private SelenideElement cityDelivery (){
        return $x("//input[@id='Order[rOrt]']");
    }

    private SelenideElement countryDelivery (String countryDelivery ){
        return $x("//*[@id='form_Order[payment_country_id]']//*[@data-code='" + countryDelivery + "']");
    }

    private SelenideElement emailDelivery (){
        return $x("//input[@id='Order[Email]']");
    }

    private SelenideElement phoneDelivery (){
        return $x("//input[@id='Order[rTelefon]']");
    }
}
