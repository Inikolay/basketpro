package aws_page;

import com.codeborne.selenide.SelenideElement;
import plus_page.CardDatUserPage;

import java.io.IOException;
import java.time.Duration;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;

public class AwsCustomerDataPage {

    private final String CUSTOMER_DATA_URL= "https://aws.autodoc.de/customer/view/";

    private SelenideElement orderIdOnTable(){
       return $x("//a[@class='order_link'][1]");
    }

    private SelenideElement customerIdBlock(){
        return $x("//td[@class='customer-id-cell']");
    }

    private SelenideElement openDropDaw(){
        return $x("//div[@class='chzn-container chzn-container-single']/a");
    }

    private SelenideElement customerSubscriptionSelector(String substationType){
        return $x("//div[contains(@id,'select_chosen_subscription')]//li[text()='"+ substationType + "']");
    }

    private SelenideElement textCustomerSubscription(){
        return $x("//div[contains(@id,'select_chosen_subscription')]");
    }


    public OrderPage clickOrderIdOnTable(){
        orderIdOnTable().shouldBe(visible).click();
        return page(OrderPage.class);
    }

    public AwsCustomerDataPage openCustomerView(String customerId) {
        AwsLogInPage logInPage = new AwsLogInPage();
        open(CUSTOMER_DATA_URL + customerId);
        if (logInPage.inputLogInAws().isDisplayed()){
            logInPage.logInOnAws();
        }
        customerIdBlock().shouldBe(visible, ofSeconds(10));
        return this;
    }

    public AwsCustomerDataPage chooseCustomerSubscriptionType(String substationType){
        openDropDaw().click();
        customerSubscriptionSelector(substationType).scrollIntoView("{block: \"center\"}").click();
        refresh();
        textCustomerSubscription().shouldHave(text(substationType), ofSeconds(10));
        return this;
    }
}
