package aws_page;

import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ProFilerPage extends OrderPage{
    private SelenideElement orderIdProFiler() {
        return $x("//input[@id='form_orderId']");
    }

    private SelenideElement articleIdProFiler() {
        return $x("//input[@id='form_orderArticleId']");
    }

    private SelenideElement btnGo() {
        return $x("//div[@class='form-group']/button[@class='btn btn-primary']");
    }

    private SelenideElement standardMultiplier(){
        return $x("//table[@class='table table-condensed table-bordered'][2]//tr/td[2]");
    }

    public ProFilerPage writeOrderIdProFiler(String orderIdAWS) {
        orderIdProFiler().setValue(orderIdAWS);
        return this;
    }

    public ProFilerPage writeArticleIdProFiler(String articleIDAWS) {
        articleIdProFiler().setValue(articleIDAWS);
        return this;
    }

    public ProFilerPage clickBtnGo() {
        btnGo().shouldBe(visible).click();
        return this;
    }

    public ProFilerPage checkStandardMultiplier(String standardMultiplier){
        String getStandardMultiplier = standardMultiplier().getText();
        Assert.assertEquals(standardMultiplier, getStandardMultiplier);
        return this;
    }
}
