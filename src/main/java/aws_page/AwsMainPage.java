package aws_page;


import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.editable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class AwsMainPage {

    private SelenideElement btnOrderSearch(){
        return $x("//div/ul/li[7]/a");
    }



    public AwsSearchOrdersPage clickBtnOrderSearch(){
        btnOrderSearch().shouldBe(visible).click();
        return page(AwsSearchOrdersPage.class);
    }
}
