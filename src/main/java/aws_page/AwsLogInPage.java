package aws_page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;
import static java.time.Duration.ofSeconds;

public class AwsLogInPage {

    public static final String EMAIL_AWS = "autodoc-13376";

    public static final String PASSWORD_AWS = "u1x/0Y/G";

    public SelenideElement inputLogInAws() {
        return $x("//input[@name='login']");
    }

    public SelenideElement inputPasswordAws() {
        return $x("//input[@name='password']");
    }

    public SelenideElement btnInAws() {
        return $x("//button[@type='submit']");
    }

    public AwsSearchOrdersPage logInAwsSearchOrders(String logInAws, String passwordAws) {
        inputLogInAws().setValue(logInAws);
        inputPasswordAws().setValue(passwordAws);
        btnInAws().click();
        return page(AwsSearchOrdersPage.class);
    }

    public AwsSearchCustomerPage logInAwsForCustomerPage() {
        inputLogInAws().shouldBe(visible, ofSeconds(10)).setValue(EMAIL_AWS);
        inputPasswordAws().setValue(PASSWORD_AWS);
        btnInAws().click();
        btnInAws().shouldNotBe(visible, ofSeconds(10));
        return page(AwsSearchCustomerPage.class);
    }

    public void logInOnAws() {
        inputLogInAws().shouldBe(visible, ofSeconds(10)).setValue(EMAIL_AWS);
        inputPasswordAws().setValue(PASSWORD_AWS);
        btnInAws().click();
        btnInAws().shouldNotBe(visible, ofSeconds(10));
    }
}
