package main_page;


import com.codeborne.selenide.SelenideElement;
import common.Cookies;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.*;


public class MainPage implements Cookies {
    private SelenideElement btnLogOut(){
        return $x("//a[@class='header__logout logout_but']");
    }

    private SelenideElement logInUserMainPage(){
        return $x("//a[@class='js-show-login-popup header-i header-i--user']");
    }

    private SelenideElement deliveryBtn(){
        return $x("//a[@data-ga-action='delivery']");
    }

    public MainPage clickBtnLogOut(){
        closeCookiesPopupBtn();
        btnLogOut().shouldBe(visible, ofSeconds(5)).click();
        btnLogOut().shouldNotBe(visible, ofSeconds(5));
        return this;
    }

    public AuthorisationPopPapLogIn clickLogInUserMainPage(){
        logInUserMainPage().shouldBe(visible).click();
        return page(AuthorisationPopPapLogIn.class);
    }

    public VersandPage clickOnDelivery(){
        deliveryBtn().scrollIntoView("{block: \"center\"}").click();
        return page(VersandPage.class);
    }

}
