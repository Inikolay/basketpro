package main_page;

import com.codeborne.selenide.SelenideElement;
import common.Cookies;
import plus_page.PlusAddressPage;
import profile_pages.ProfilePage;

import java.time.Duration;

import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class AuthorisationPopPapLogIn implements Cookies {
    private SelenideElement emailUser(){
        return $x("//input[@id='login_top_email']");
    }
    private SelenideElement passwordUser(){
        return $x("//input[@type='password']");
    }

    private SelenideElement btnLogIn(){
        return $x("//div[@class='button']/a");
    }

    public PlusAddressPage logInUserlogInUserPlusAddressPage(String email, String password){
        emailUser().shouldBe(visible, Duration.ofSeconds(10)).setValue(email);
        emailUser().shouldHave(value(email));
        passwordUser().setValue(password);
        passwordUser().shouldHave(value(password));
        btnLogIn().click();
        return page(PlusAddressPage.class);
    }

    public ProfilePage logInUserInUserProfilePage(String email, String password){
        emailUser().shouldBe(visible, Duration.ofSeconds(10)).setValue(email);
        emailUser().shouldHave(value(email));
        passwordUser().setValue(password);
        passwordUser().shouldHave(value(password));
        btnLogIn().click();
        return page(ProfilePage.class);
    }
}
