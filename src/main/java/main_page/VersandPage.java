package main_page;

import com.codeborne.selenide.SelenideElement;


import static com.codeborne.selenide.Selenide.$x;


public class VersandPage {



    private SelenideElement deliveryPrice(){
        return $x("//div[@class='prices-block__items-item']/span[@data-image='image1']");
    }

    private SelenideElement safeOrderPrice(){
        return $x("(//div[@class='safe-order__number-row']/p[@class='price'])[2]");
    }

    public float getSafeOrderPrice(){
        return Float.parseFloat(safeOrderPrice().scrollIntoView("{block: \"center\"}")
                .getText().replaceAll("[^\\d,]", "").replaceAll(",", "."));
    }

    public String getDeliveryPrice(){
        String getDeliveryPriceItem = deliveryPrice().getText().replaceAll("\\D\\B", "");
        return getDeliveryPriceItem;
    }


}
